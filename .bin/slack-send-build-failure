#!/usr/bin/env bash

# shellcheck source=./lib/bash/core.sh
source "${BASH_SOURCE[0]%/*}/../lib/bash/core.sh"

function main() {

  local JENKINS_URL="${JENKINS_URL:-https://jenkins.nabstudio.com/}"
  local JOB_NAME="${JOB_NAME:-nabstudio%2Funknown}"
  local GIT_COMMITTER_NAME="${GIT_COMMITTER_NAME:-Unknown}"
  local GIT_COMMITTER_EMAIL="${GIT_COMMITTER_EMAIL:-unknown@unknown.com}"

  local SLACK_WEBHOOK_URL="${SLACK_WEBHOOK_URL:-}"
  local SLACK_TOKEN="${SLACK_TOKEN:-}"

  local SLACK_USER_ID
  SLACK_USER_ID="$(
    curl \
      --silent \
      "https://slack.com/api/users.lookupByEmail?token=${SLACK_TOKEN}&email=${GIT_COMMITTER_EMAIL}" |
      jq \
        --raw-output \
        ".user.id"
  )"

  if [[ "${SLACK_USER_ID}" == "null" && "${GIT_COMMITTER_EMAIL}" == *"notabasement.com" ]]; then
    SLACK_USER_ID="$(
      curl \
        --silent \
        "https://slack.com/api/users.lookupByEmail?token=${SLACK_TOKEN}&email=${GIT_COMMITTER_EMAIL/notabasement.com/nabstudio.com}" |
        jq \
          --raw-output \
          ".user.id"
    )"
  fi

  if [[ "${SLACK_USER_ID}" == "null" ]]; then
    SLACK_USER_ID=""
  fi

  local MENTION=""
  if [[ -n "${SLACK_USER_ID}" ]]; then
    MENTION="<@${SLACK_USER_ID}>"
  else
    MENTION="${GIT_COMMITTER_NAME}"
  fi

  local DATA
  DATA="$(
    echo '{}' |
      jq \
        --compact-output \
        --arg JENKINS_URL "${JENKINS_URL}" \
        --arg JOB_NAME "${JOB_NAME/\%2F//}" \
        --arg JOB_URL "${JOB_URL:-${JENKINS_URL}}" \
        --arg BUILD_NUMBER "${BUILD_NUMBER:-0}" \
        --arg BUILD_URL "${BUILD_URL:-${JENKINS_URL}}" \
        --arg BUILD_USER_ID "${BUILD_USER_ID:-jenkins}" \
        --arg BUILD_USER "${BUILD_USER:-Jenkins}" \
        --arg RUN_DISPLAY_URL "${RUN_DISPLAY_URL:-${JENKINS_URL}}" \
        --arg GIT_COMMITTER_NAME "${GIT_COMMITTER_NAME}" \
        --arg GIT_COMMITTER_EMAIL "${GIT_COMMITTER_EMAIL}" \
        --arg MENTION "${MENTION}" \
        '.main = {
          "attachments": [
            {
              "color": "danger",
              "fallback": "\($JOB_NAME) #\($BUILD_NUMBER) by \($GIT_COMMITTER_NAME) failed.",
              "text": "*<\($JOB_URL)|\($JOB_NAME)> <\($BUILD_URL)|#\($BUILD_NUMBER)>* by *<\($JENKINS_URL)user/\($BUILD_USER_ID)|\($BUILD_USER)>* failed.\n[<\($BUILD_URL)console|Console>|<\($RUN_DISPLAY_URL)|BlueOcean>]\n\n*Git Author*\n\($MENTION) \($GIT_COMMITTER_EMAIL)",
            }
          ]
        } | .main'
  )"
  # echo "DATA = '${DATA}'"

  (
    # set -x
    export SLACK_WEBHOOK_URL="${SLACK_WEBHOOK_URL}"
    slack-send "${DATA}"
  )

  # echo

}

main "$@"
