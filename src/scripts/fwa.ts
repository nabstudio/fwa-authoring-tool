import { S3 } from "aws-sdk";
import { createReadStream, promises as fs } from "fs";
import { lookup } from "mime-types";
import * as pathLib from "path";
// @ts-ignore Wrong error
import yargs from "yargs";

// (./pipeline/debug ts-node) ./src/scripts/fwa.ts [--input|-i /path/to/input/folder]
// [--key|-k s3-promotion-key]
// [--output {s3, folder}]
// [--url url]

// Env Vars:
// - AWS_ACCESS_KEY_ID
// - AWS_SECRET_ACCESS_KEY

// HELPERS
async function readFileToString(path: string) {

  const buf = await fs.readFile(path);
  return buf.toString();
}

async function pathExists(path: string) {
  try {
    await fs.access(path);
    return true;
  } catch (err: unknown) {
    return false;
  }
}

async function errorOnBool(bool: Promise<boolean>, msg: string) {
  const b = await bool;
  if (!b) {
    throw new Error(msg);
  }

}

async function pathErrorOnBool(bool: Promise<boolean>) {
  return errorOnBool(bool, "Path Error.");
}

function insertBeforeSecondLastOccurrence(strToSearch: string, strToFind: string, strToInsert: string) {
  let n = strToSearch.lastIndexOf(strToFind);
  n = strToSearch.lastIndexOf(strToFind, n - 1);
  if (n < 0) return strToSearch;
  return strToSearch.substring(0, n) + strToInsert + strToSearch.substring(n);
}

function insertBeforeLastOccurrence(strToSearch: string, strToFind: string, strToInsert: string) {
  const n = strToSearch.lastIndexOf(strToFind);
  if (n < 0) return strToSearch;
  return strToSearch.substring(0, n) + strToInsert + strToSearch.substring(n);
}

function insertAfterLastOccurrence(strToSearch: string, strToFind: string, strToInsert: string) {
  const n = strToSearch.lastIndexOf(strToFind) + strToFind.length;
  if (n < 0) return strToSearch;
  return strToSearch.substring(0, n) + strToInsert + strToSearch.substring(n);
}

// Recursive getFiles from
// https://stackoverflow.com/a/45130990/831465
async function getFiles(dir: string): Promise<string | string[]> {
  const dirents = await fs.readdir(dir, { withFileTypes: true });
  const files = await Promise.all(
    dirents.map((dirent) => {
      const res = pathLib.resolve(dir, dirent.name);
      return dirent.isDirectory() ? getFiles(res) : res;
    }),
  );
  return Array.prototype.concat(...files);
}

async function copyFile(src: string, dst: string) {
  const folders = dst.split("/");
  const dstFolder = folders.slice(0, folders.length - 1).join("/");
  await fs.mkdir(dstFolder, { recursive: true });
  return fs.copyFile(src, dst);
}

async function copyDir(srcDir: string, dstDir: string) {
  await fs.mkdir(dstDir, { recursive: true });
  const files = await getFiles(srcDir);

  if (Array.isArray(files)) {
    await Promise.all(files.map((src) => copyFile(src, `${dstDir}/${pathLib.relative(srcDir, src)}`)));
    return;
  }

  await fs.copyFile(srcDir, dstDir + pathLib.relative(srcDir, files));
}
// S3

const s3Client = new S3({ region: "us-east-1" });

async function uploadDir(path: string, s3Path: string, bucketName: string) {
  const s3 = new S3();

  const files = (await getFiles(path)) as string[];
  const uploads = files.filter((file) => !file.includes(".DS_Store")).map((filePath) => s3
    .putObject({
      Key: `${s3Path}/${path.split("/").slice(-1)}/${pathLib.relative(path, filePath)}`,
      Bucket: bucketName,
      Body: createReadStream(filePath),
      ContentType: lookup(filePath) as string,
    })
    .promise());
  return Promise.all(uploads);
}

async function uploadFile(str: string, s3Path: string, bucketName: string) {
  return s3Client.putObject({
    Key: s3Path,
    Bucket: bucketName,
    Body: str,
    ContentType: lookup(s3Path) as string,
  }).promise();
}

// DOMAIN LOGIC

const BUCKET = "mailtrain-files.inkr.com";
const MAIN_FOLDER = "promo-events";

export async function uploadDirToPromo(dirPath: string, promoName: string, subKey: string) {
  return uploadDir(dirPath, `${MAIN_FOLDER}/${promoName}/${subKey}`, BUCKET);
}

function toApp1(html: string, url: string): string {
  console.log("url", url);

  let res = html.replace(
    /background-color:#e5e5e5/g,
    "background-color:#000000",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"non-member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = res.replace(
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">",
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, viewport-fit=cover\">",
  );

  res = insertBeforeLastOccurrence(
    res,
    "</head>",
    `
    <!-- BEGIN FWA CUSTOM -->
    <link href="common/css/article-common.css" rel="stylesheet" />
    <style type="text/css">
      /* BEGIN CUSTOM STYLES - change [.header-bar-td background] to modify Header Background */
      .fake-header-bar-td {
        height: env(safe-area-inset-top) !important;
        background-color: #000000;
      }

      .header-bar-td {
        height: 56px !important;
        background-color: #000000;
      }

      /* NProgress */
      #fwa-progress-bar.bar {
        background-color: #ffd60a;
      }
      /* END CUSTOM STYLES */
    </style>
    <script type="text/javascript" src="common/js/article-common.js"></script>
    <script type="text/javascript" src="common/js/article-app.js" defer></script>
    <!-- END FWA CUSTOM -->
    `,
  );

  res = insertAfterLastOccurrence(
    res,
    "<div style=\"background-color:#000000;\">",
    `
    <!-- BEGIN HEADER -->
    <div id="header-bar-wrapper" style="margin:0px auto;border-radius:0; padding: 0;">
      <div style="width: 100%; max-width: 600px; margin: 0 auto;">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
          style="background:#ffffff;background-color:#ffffff;border-radius:0;width: 100%; max-width: 600px;">
          <tbody>
            <tr>
              <td class="fake-header-bar-td" style="border:none;direction:ltr;font-size:0;padding:0;text-align:left;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td class="header-bar-td" height="56"
                style="height:56px;border:none;direction:ltr;font-size:0;padding:0;line-height: 1px; width: 100%; max-width: 600px;">
                <img alt="" width="24" height="24"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA/CAYAAABXXxDfAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAYaSURBVHgB1VtBVttIEC3xssUXSA6AL+DZMz5AzD7hADh7AnvIHjx7D+vAHMBv1gOzH2f28QFCDkD6SyVHln9J3VLLcv57/XhYUqt+V3V1VXUrkY7x8vIycn+G2l5rO3RtULjt2bXvrq1cW+Z/kyR5kg6RSGQ4siB24trYtSPZJBkKDAoGYIHmBuO7REQ08qphEJ5IO8JVeHDtxg3CSiKgNXkl/cG1kewOsIbztoPQmLya96Vkmu4LrSyhEXlHHHP6o4SZ91J+OrOysOgHg5k7xaH4A31hAB4kEEHkVdsw8VOP2+GsIBCc1TLEWel7MACwqt8kG5A6zCUbBO/3eJN3AkGAmdRr5dG125jLlHs3BiEfiCrACt75TgMv8kr8Tqo1EJ00kQNO9VONHN4DUEvegzjMG6TnsiM4mTDtpmL7HK8BSGpeUkccDuws1robAg/ZagfgoKLzw5rO567jt30QB/Be144lc3QMqY9SHhQmecm8ukUcXvVK9gAqx41xGc75g/UsJa/e1VrOQPxW9ggqjzUAp8pn+7nyDzVzab4vGmdwsl8IVxqc8nE5BmCat8x9tc/EAZVvSS5hVbgs/7hBXrXOTAQj905+DZxJJm8ZE40T1ihr/pNwXPXl1UOhclo+acP5rcmr1llaumqSNPQJDbhYpDkqav9V4YK1JJyLJ3RNLUddz00qMBH6gve/I7+j4JIOTKIvwkv+JTc+upe9F0+4fmbaeRGYf0HBUMWKE7TauH7Qx4jIk3r+3Ox/N54PNXfk+CxXv6uKtIqoII5+Q/MHtvZDHtQj1nN+TG4Knuvu/nxVKHtbEPnDrxeatQWlqgV5noR7/pRvTp45ukdpABXwjFwaaRBiwl23aoFt6nVMgagqy4F6v4HnQ17QEWdz81TT0S0o8Sm5dNOyRrAgvw3AG5q3KjNLaQFdbtgcvSgHG+5/mKFFvG0e8cX4fQjyLJQNqrlZUM/MtDZTx5Y7uGtyzyJGAqV+iCnyyNJ8zGjuTOwVAO++k+1ph/u94wsPMPKvTc1LJNSsAH+J7dljbk0xZb4Bebb+xt0Tyzy1b7DURVmM9XcI8szTs7WxFRwhWFNddHal9+0CgwPZIXQFuDcu3++yAgzslLx69rFxeZyvALsCyEc3cYZCzG7V2vMVoIsBoFMb5Jlz60KAmUe/teXmhqBOHeRXhhDRoDF9OZ6AxZ3LtuXhvkuJCxbLfAX5r543N4LG7Cyev9Wska0AE30uFpgyv4A8i32HMUyvsKdWxk3u2XUAWN49tertgTJgvtMoFuStdbWV9tVxMeLzcsyu/7Ms8kJD4DY4Mn5fHtQl/E1Q4dnTUxTGY6zmjudnLVeAE/IbaoFP+TrPTL+RyVVscFbG7JoDsCSo8QrgnsEfVhxJM82cvJnwSzhYGSpNbmr3y39WgWKtABPhzi7lm5N/EG76QR5XPTSbLh+9j4rYOcDEqgIZsuDPiXE5Jf9KX/jsbsYAlDtPi/wBZSQIXp7TKIQuJABYAVT4stZCylk4v8Ms93M+9da7tGrirMj/5G7+VfbpUujA/S3c5I9zK1wnNqpda4vH29z6hhKHvIz4P8Xpl5QetLQfvOvSFxyHN5L5MJbMHBc5bKS0qn2Wb6frrew5VOt/Cif+uaw8ls+jkso8/7Bu06FPKHHIRw9WCFHeFnkNNqxy02nkhCMKlHjVsVh6OJlWcjTZsEpK030agALxqXHL3NpzTCo6xbzB/BlWdNrbGR0lDfmxM2xpHOn6WzOkluoXtD7l2AWUOLw6skFLOSD+vtEJTEAfRIBjdZBuPOwqDgBp16AwvA+m3Jg4EPPUdbrFlHRw6rpg4ohDplL9SYsX8bxDXwF8BgBIY4Uk0iEm1bQPaeA/3Bf1vH1BkIEK4WPmECAPmpp+aeH7tRZMA865my8tSsJBoKqDyQzrjwUlK5ezjUs4MZSdQkpX31y77vwbmyIKNboT6QfQNqzquumObozv6vLDBXXfv8QCSOO80Kytc435RWXXlgDzxn7+ItaK0tW3tGNt1mEnr64k8wvwEShM3Cf7+i2thcJX1LCMI/07kM1BgVZBNP/g8H/ZwVfUPwALUJN6zBo87gAAAABJRU5ErkJggg=="
                  style="border:0;border-radius:0;display:block;outline:none;text-decoration:none;margin-left: 24px;margin-top:16px;margin-bottom: 16px;"
                  onclick="if(window.JSBridge){window.JSBridge['closeWindow']();}else{window.webkit.messageHandlers['closeWindow'].postMessage('closeWindow');}" />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div
      style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0;padding: 0;width: 100%; max-width: 600px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0"
        style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0;">
        <tbody>
          <tr>
            <td class="fake-header-bar-td" style="border:none;direction:ltr;font-size:0;padding:0;text-align:left;">
              &nbsp;
            </td>
          </tr>

          <tr>
            <td class="header-bar-td" height="56"
              style="height:56px;border:none;direction:ltr;font-size:0;padding:0;text-align:left;line-height: 1px;">
              &nbsp;
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- END HEADER -->
    `,
  );

  // res = insertBeforeSecondLastOccurrence(
  //   res,
  //   "</td></tr></tbody>",
  //   `
  //   <div style="height:20px">&nbsp;</div><div class="sharethis-inline-share-buttons" data-url="${url}"></div>
  //   `,
  // );

  return res;
}

function toApp2(html: string, url: string): string {
  console.log("url", url);

  let res = html.replace(
    /background-color:#e5e5e5/g,
    "background-color:#000000",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"non-member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = insertBeforeLastOccurrence(
    res,
    "</head>",
    `
    <!-- BEGIN FWA CUSTOM -->
    <link href="common/css/article-common.css" rel="stylesheet" />
    <style type="text/css">
      /* BEGIN CUSTOM STYLES - change [.header-bar-td background] to modify Header Background */
      .fake-header-bar-td {
        height: env(safe-area-inset-top) !important;
        background-color: #000000;
      }

      .header-bar-td {
        height: 56px !important;
        background-color: #000000;
      }

      /* NProgress */
      #fwa-progress-bar.bar {
        background-color: #ffd60a;
      }
      /* END CUSTOM STYLES */
    </style>
    <script type="text/javascript" src="common/js/article-common.js"></script>
    <script type="text/javascript" src="common/js/article-app.js" defer></script>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TG12B7D57M"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', 'G-TG12B7D57M');
    </script>
    <!-- END FWA CUSTOM -->
    `,
  );

  // res = insertBeforeSecondLastOccurrence(
  //   res,
  //   "</td></tr></tbody>",
  //   `
  //   <div style="height:20px">&nbsp;</div><div class="sharethis-inline-share-buttons" data-url="${url}"></div>
  //   `,
  // );

  return res;
}

function toWeb(html:string, url: string): string {

  let res = html.replace(
    /background-color:#e5e5e5/g,
    "background-color:#000000",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"non-member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = res.replace(
    /div class="r pt-10 pr-22 pb-0 pl-22 "/,
    "div id=\"member\" class=\"r pt-10 pr-22 pb-0 pl-22 \"",
  );

  res = insertBeforeLastOccurrence(
    res,
    "</head>",
    `
    <!-- BEGIN FWA CUSTOM -->
    <link href="common/css/article-common.css" rel="stylesheet" />
    <style type="text/css">
      /* BEGIN CUSTOM STYLES - change [.header-bar-td background] to modify Header Background */
      .fake-header-bar-td {
        height: env(safe-area-inset-top) !important;
        background-color: #000000;
      }

      .header-bar-td {
        height: 56px !important;
        background-color: #000000;
      }

      /* NProgress */
      #fwa-progress-bar.bar {
        background-color: #ffd60a;
      }
      /* END CUSTOM STYLES */
    </style>
    <script type="text/javascript" src="common/js/article-common.js"></script>
    <script type="text/javascript" src="common/js/article-web.js" defer></script>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TG12B7D57M"></script>

    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', 'G-TG12B7D57M');
    </script>
    ${url ? "<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=6177e56b7c83ea00126753af&product=inline-share-buttons' async='async'></script>" : ""}
    <!-- END FWA CUSTOM -->
    `,
  );

  if (url) {
    res = insertBeforeSecondLastOccurrence(
      res,
      "</td></tr></tbody>",
      `
      <div style="height:20px">&nbsp;</div><div class="sharethis-inline-share-buttons" data-url="${url}"></div>
      `,
    );
  }

  return res;
}

async function cleanCompressedImages(pathDir: string) {
  const files = await getFiles(pathDir);
  const isUnclean = (filePath: string) => filePath.includes("-min");
  const rename = async (filePath: string) => {
    await fs.rename(filePath, filePath.replace("-min", ""));
  };
  if (Array.isArray(files)) {
    await Promise.all(files.filter(isUnclean).map(rename));
    return;
  }

  if (isUnclean(files)) {
    await rename(files);
  }
}

export async function uploadApp1(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const s3Path = `${MAIN_FOLDER}/${promo}/app-1`;
  await uploadDir(imgPath, s3Path, BUCKET);
  await uploadDir(publicPath, s3Path, BUCKET);
  await uploadDir("./src/common", s3Path, BUCKET);
  if (css !== undefined) {
    await uploadFile(css, `${s3Path}/index.css`, BUCKET);
  }
  await uploadFile(toApp1(html, url), `${s3Path}/index.html`, BUCKET);
}

export async function uploadApp2(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const s3Path = `${MAIN_FOLDER}/${promo}/app-2`;
  await uploadDir(imgPath, s3Path, BUCKET);
  await uploadDir(publicPath, s3Path, BUCKET);
  await uploadDir("./src/common", s3Path, BUCKET);
  if (css !== undefined) {
    await uploadFile(css, `${s3Path}/index.css`, BUCKET);
  }
  await uploadFile(toApp2(html, url), `${s3Path}/index.html`, BUCKET);
}

export async function uploadWeb(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const s3Path = `${MAIN_FOLDER}/${promo}/web`;
  await uploadDir(imgPath, s3Path, BUCKET);
  await uploadDir(publicPath, s3Path, BUCKET);
  await uploadDir("./src/common", s3Path, BUCKET);
  if (css !== undefined) {
    await uploadFile(css, `${s3Path}/index.css`, BUCKET);
  }
  await uploadFile(toWeb(html, url), `${s3Path}/index.html`, BUCKET);
}

export async function prepApp1(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const path = `./${promo}/app-1`;
  await copyDir(imgPath, `${path}/img`);
  await copyDir(publicPath, `${path}/public`);
  await copyDir("./src/common", `${path}/common`);
  if (css !== undefined) {
    fs.writeFile(`${path}/index.css`, css);
  }
  await fs.writeFile(`${path}/index.html`, toApp1(html, url));
}

export async function prepApp2(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const path = `./${promo}/app-2`;
  await copyDir(imgPath, `${path}/img`);
  await copyDir(publicPath, `${path}/public`);
  await copyDir("./src/common", `${path}/common`);
  if (css !== undefined) {
    fs.writeFile(`${path}/index.css`, css);
  }
  await fs.writeFile(`${path}/index.html`, toApp2(html, url));
}

export async function prepWeb(html:string, css: string, imgPath: string, publicPath: string, promo: string, url: string) {
  const path = `./${promo}/web`;
  await copyDir(imgPath, `${path}/img`);
  await copyDir(publicPath, `${path}/public`);
  await copyDir("./src/common", `${path}/common`);
  if (css !== undefined) {
    fs.writeFile(`${path}/index.css`, css);
  }
  await fs.writeFile(`${path}/index.html`, toWeb(html, url));
}

// SCRIPTx

async function main() {

  const { argv } = yargs(process.argv.slice(2)) as {argv: Record<string, string>};

  const {
    input: path,
    key: promo,
    output,
    url,
    title,
  } = argv;

  let outType: string;
  outType = output;
  if (!output) {
    outType = "s3";
  }

  if (!path || !promo) {
    throw new Error("Invalid param");
  }

  await pathErrorOnBool(pathExists(path));
  const imgPath = `${path}/img`;
  const publicPath = `${path}/public`;
  const indexHtmlPath = `${path}/index.html`;
  const indexCssPath = `${path}/index.css`;
  await pathErrorOnBool(pathExists(imgPath));
  await pathErrorOnBool(pathExists(indexHtmlPath));

  const indexHTML = await readFileToString(indexHtmlPath);
  const indexCSS = await readFileToString(indexCssPath);
  // await fs.writeFile("./test.html", toApp1(indexHTML));
  // update;
  // await uploadDirToPromo("./src/common", "testFWAScript");
  await cleanCompressedImages(imgPath);


  if (outType === "s3") {

    await uploadApp1(indexHTML, indexCSS, imgPath, publicPath, promo, url);
    await uploadApp2(indexHTML, indexCSS, imgPath, publicPath, promo, url);
    await uploadWeb(indexHTML, indexCSS, imgPath, publicPath, promo, url);

    console.log("");
    console.log("Test URLs:");
    console.log(`Web: https://inkr.com/fwa?url=https://mailtrain-files.inkr.com/promo-events/${promo}/web${
      title ? `&title=${encodeURIComponent(title)}` : ""
    }`);
    console.log(`Callback: https://inkr.com/callback?redirect=${
      encodeURIComponent(`https://inkr.com/fwa?url=https://mailtrain-files.inkr.com/promo-events/${promo}/web${
        title ? `&title=${encodeURIComponent(title)}` : ""
      }`)
    }`);
    console.log(`App1: com.inkr.comics://fwa?url=https://mailtrain-files.inkr.com/promo-events/${promo}/app-1`);
    console.log(`App2: com.inkr.comics://fwa?url=https://mailtrain-files.inkr.com/promo-events/${promo}/app-2`);

  } else if (outType === "folder") {

    await prepApp1(indexHTML, indexCSS, imgPath, publicPath, promo, url);
    await prepApp2(indexHTML, indexCSS, imgPath, publicPath, promo, url);
    await prepWeb(indexHTML, indexCSS, imgPath, publicPath, promo, url);

    console.log("");
    console.log("Test URLs:");
    console.log(`Web:  file://${pathLib.resolve(__dirname, "../..")}/${promo}/web/index.html`);
    console.log(`App1: file://${pathLib.resolve(__dirname, "../..")}/${promo}/app-1/index.html`);
    console.log(`App2: file://${pathLib.resolve(__dirname, "../..")}/${promo}/app-2/index.html`);
  }
}

main()
  .then(() => {
    process.exit(0);
  })
  .catch((reason) => {
    console.error(reason);
    process.exit(1);
  });
