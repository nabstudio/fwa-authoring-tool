// Command-line to generate Retool string into clipboard:
// cat ./src/common/js/article-app.js | sed -e 's|\\|\\\\|g' -e 's|`|\\`|g' -e 's|${|$\\{|g' | pbcopy

/**
   * @param {string} url
   * @param {string} key
   * @param {string} value
   */
 function addQueryParam(url, key, value) {
  const newUrl = new URL(url);
  newUrl.searchParams.set(key, value);
  return newUrl.toString();
}

const appAElements = document.getElementsByTagName("a");

for (let index = 0; index < appAElements.length; index++) {

  const element = appAElements.item(index);
  if (!element) continue;

  let href = element.getAttribute("href") || element.getAttribute("xlink:href") || "";

  if (href === "") {
    continue;
  }

  if (href.includes("com.inkr.comics://")) {
    continue;
  }

  if (href.includes("share:")) {
    const path = href.replace("share://", "");
    const t = encodeURIComponent(`https://${path}`);
    let al = undefined;

    // @ts-ignore window.inkrAnalyticsTracking
    if (typeof window.inkrAnalyticsTracking === "string" && window.inkrAnalyticsTracking.trim()) {
      // @ts-ignore window.inkrAnalyticsTracking
      al = window.inkrAnalyticsTracking.trim();
    }

    const finalLink = `com.inkr.comics://shareLink?url=${t}${al ? `&analyticLocation=${al}` : ""}`
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", finalLink);
    } else {
      element.setAttribute("xlink:href", finalLink);
    }
    continue;
  }

  // @ts-ignore window.inkrAnalyticsTracking
  if (typeof window.inkrAnalyticsTracking === "string" && window.inkrAnalyticsTracking.trim()) {
    // @ts-ignore window.inkrAnalyticsTracking
    href = addQueryParam(href, "analyticLocation", window.inkrAnalyticsTracking.trim());
  }

  const query = href.replace(/^[^?]*\??(.*)/, "$1");

  if (href.includes("/membership-landing")) {
    const path = "com.inkr.comics://inkrExtra";
    const newHref = `${path}${query ? `?${query}` : ""}`;
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.includes("inkr.com/earn-ink")) {
    const path = "com.inkr.comics://earnink";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  if (href.includes("inkr.com/about-ink")) {
    const path = "com.inkr.comics://aboutInk";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  if (href.includes("inkr.com/comics-reel")) {
    const path = "com.inkr.comics://comicsReel";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  if (href.includes("inkr.com/weekly-free-ie-unlock")) {
    const path = "com.inkr.comics://weeklyFreeIEUnlock";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  if (href.includes("inkr.com/manhua-reel")) {
    const path = "com.inkr.comics://manhuaReel";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  if (href.includes("inkr.com/explore/read-with-ink") || href.includes("inkr.com/browse/read-with-ink")) {
    const path = "com.inkr.comics://explorePath?path=readWithInk";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", path);
    } else {
      element.setAttribute("xlink:href", path);
    }
    continue;
  }

  // Convert all "More About Ink" on web to App version when open on mobile
  if (href.includes("in-web-fwa-info/about-ink/") || href.includes("in-web-fwa-info%2Fabout-ink%2F")) {
    const newHref = "com.inkr.comics://fwa?url=https://mailtrain-files.inkr.com/in-app-fwa-info/about-ink/&title=Read%20with%20Ink%20&showTopBar=true&navigation=modal&backgroundHexColor=000000";
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/claim-gift-code\?/)) {
    const newHref = href.replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/claim-gift-code\?/, "com.inkr.comics://giftCode?");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-subscribe\?/)) {
    const newHref = href.replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-subscribe\?/, "com.inkr.comics://titleSubscribe?");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-like\?/)) {
    const newHref = href.replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-like\?/, "com.inkr.comics://titleLike?");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-read-later\?/)) {
    const newHref = href.replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/action\/title-read-later\?/, "com.inkr.comics://titleReadLater?");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/title-listing\?/)) {
    const newHref = href.replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/title-listing\?/, "com.inkr.comics://titleListing?");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/fwa\?url=/)) {
    const newHref = href
      .replace(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\/fwa\?/, "com.inkr.comics://fwa?")
      .replace(/\+/g, "%20");
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.includes("/title/")) {
    const path = href.replace(/^.*\/title\/([0-9]+).*/, "com.inkr.comics://titleInfo?id=ik-title-$1");
    const newHref = `${path}${query ? `&${query}` : ""}`;
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  if (href.match(/^https?:\/\/([0-9a-zA-Z-]+\.)?inkr.com\//)) {
    const newHref = `com.inkr.comics://webAutoLogin?url=${encodeURIComponent(href)}`;
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", newHref);
    } else {
      element.setAttribute("xlink:href", newHref);
    }
    continue;
  }

  const newHref = `com.inkr.comics://web?url=${encodeURIComponent(href)}`;
  if (element.getAttribute("href") !== undefined) {
    element.setAttribute("href", newHref);
  } else {
    element.setAttribute("xlink:href", newHref);
  }
}
