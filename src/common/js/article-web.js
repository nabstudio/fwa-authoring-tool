function addQueryParam(url, key, value) {
  const newURL = new URL(url);
  newURL.searchParams.set(key, value);
  return newURL.toString();
}

function postMessageToHandleURL(url) {
  if (typeof window === "undefined") return;

  let payload = {
    type: "fwa-request-message",
    command: "handleURL",
    url: url,
  }

  if (url.includes("/action/title-subscribe")) {
    const tid = url.replace(/^.*\/action\/title-subscribe\?id=ik-title-([0-9]+).*/, "$1");
    payload = {
      type: "fwa-request-message",
      command: "titleSubscribe",
      tid: tid,
    }
  }

  try {
    window.parent.postMessage(payload, "*");
  } catch (error) {
    console.error("[postMessageToHandleURL] error", error);
  }
};

const shareFunction = (url) => {
  if (navigator.share) {
    navigator.share({
      url: url,
    })
      .then(() => console.log('Successful share'))
      .catch((error) => console.log('Error sharing', error));
  }
}

const elements = document.getElementsByTagName("a");

for (let index = 0; index < elements.length; index++) {
  const element = elements.item(index);

  let href = element.getAttribute("href") || element.getAttribute("xlink:href");

  if (href.includes("share:")) {
    const path = href.replace("share://", "");
    console.log(path);
    const t = `shareFunction('https://${path}');return false;`;
    console.log(t);
    if (element.getAttribute("href") !== undefined) {
      element.setAttribute("href", "#");
    } else {
      element.setAttribute("xlink:href", "#");
    }
    element.setAttribute("onClick", t)
    continue;
  }

  if (href.includes("com.inkr.comics://earnink")) {
    href = "https://inkr.page.link/webdownload";
  } else {
    href = href.replace(/^https?:\/\/(staging\.|beta\.)?inkr\.(com|dev)/, "https://inkr.com");
    if (typeof window.inkrAnalyticsTracking === "string" && window.inkrAnalyticsTracking.trim()) {
      href = addQueryParam(href, "ref", window.inkrAnalyticsTracking.trim());
    }
  }

  if (element.getAttribute("href") !== undefined) {
    element.setAttribute("href", href);
  } else {
    element.setAttribute("xlink:href", href);
  }

  element.onclick = (event) => {
    event.preventDefault();
    if (href.includes("#insertUrlLink")) return;
    postMessageToHandleURL(href);
  }
}
