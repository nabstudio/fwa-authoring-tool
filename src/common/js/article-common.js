(function main() {
  // WEB-APP-HANDLERs
  // Helpers
  function getWindowQuery(key) {
    if (typeof window === "undefined") return "";
    var queryParams = new URLSearchParams(window.location.search);
    return (queryParams.get(key) || "").toLowerCase();
  }

  function isAndroidSupported() {
    if (typeof window === "undefined") return false;
    var platform = getWindowQuery("platform");
    return (platform === "android") || !!window.JSBridge;
  };

  function isIOSSupported() {
    if (typeof window === "undefined") return false;
    var platform = getWindowQuery("platform");
    return (platform === "ios") || !!(
      window.webkit &&
      window.webkit.messageHandlers
    );
  };

  function isExtraSubscriber(_date) {
    if (!_date) return false;
    var _subscribeExpiredDate = (new Date(_date)).toISOString();
    var _now = (new Date()).toISOString();
    return _subscribeExpiredDate >= _now;
  };

  function handleReceiveUserInfo(userInfo) {
    if (!userInfo) {
      return;
    }

    var memberDiv = document.getElementById("member");
    var nonMemberDiv = document.getElementById("non-member");
    var isExtraUser = isExtraSubscriber(userInfo.extraSubscriberExpirationDate);

    if (isExtraUser) { // If MEMBER
      if (memberDiv) {
        memberDiv.style.display = "block";
      }
      if (nonMemberDiv) {
        nonMemberDiv.style.display = "none";
      }
    } else { // If NON-MEMBER
      if (memberDiv) {
        memberDiv.style.display = "none";
      }
      if (nonMemberDiv) {
        nonMemberDiv.style.display = "block";
      }
    }
  }

  function handleReceiveFirebaseToken(newFbToken) { // FAKE HANDLER
    console.debug("[handleReceiveFirebaseToken]", newFbToken);
  }
  // Helpers

  // Native-to-Web Functions
  function setUserInfo(messageUDID, jsonData) {
    if (!messageUDID || !jsonData) {
      return;
    }

    var sanitizedJSONString = jsonData.replace(/'/g, "\"");
    var parsedJSON = JSON.parse(sanitizedJSONString);

    try {
      handleReceiveUserInfo(parsedJSON);
    } catch (error) {
      console.error("nativeSetUserInfo error", error);
    }
  };

  function setFirebaseToken(messageUDID, fbToken) { // FAKE HANDLER
    console.debug("[setFirebaseToken", messageUDID, fbToken);
  };

  function setFWATheme(messageUDID, theme) { // FAKE HANDLER
    console.debug("[setFWATheme", messageUDID, theme);
  };
  // Native-to-Web Functions

  // IFrame Handlers
  function postMessageToGetUserInfo(messageUDID) {
    if (typeof window === "undefined") return;
    var command = "getUserInfo";
    try {
      window.parent.postMessage({
        type: "fwa-request-message",
        messageUDID: messageUDID,
        command: command,
      }, '*');
    } catch (error) {
      console.error("[postMessageToGetUserInfo] error", error);
    }
  };

  function postMessageToCloseWindow() {
    if (typeof window === "undefined") return;
    var command = "closeWindow";
    try {
      window.parent.postMessage({
        type: "fwa-request-message",
        command: command,
      }, '*');
    } catch (error) {
      console.error("[postMessageToCloseWindow] error", error);
    }
  };

  function postMessageToLogFwaEvent(name, params) {
    if (typeof window === "undefined") return;
    var command = "logEvent";
    try {
      window.parent.postMessage({
        type: "fwa-request-message",
        command: command,
        data: {
          name: name,
          params: params,
        },
      }, '*');
    } catch (error) {
      console.error("[postMessageToLogFwaEvent] error", error);
    }
  }

  function handleReceiveFwaMessage(event) {
    if (!event.data) {
      return;
    }
    var messageType = event.data.type;
    if (!messageType || messageType !== "fwa-response-message") {
      return;
    }
    var messageCommand = event.data.command;
    if (!messageCommand) {
      return;
    }

    var _data = event.data.data;
    switch (messageCommand) {
      // Set User Info
      case "setUserInfo": {
        if (_data) {
          handleReceiveUserInfo(_data);
        }
        break;
      }

      // Set Firebase Token
      case "setFirebaseToken": {
        if (_data) {
          handleReceiveFirebaseToken(_data);
        }
        break;
      }

      default:
        break;
    }
  }
  // IFrame Handlers


  // Wrapped Functions
  function getUserInfo(messageUDID) {
    try {
      if (isAndroidSupported()) {
        if (typeof window.JSBridge.getUserInfo === "function") {
          window.JSBridge.getUserInfo(messageUDID);
        }
      }

      if (isIOSSupported()) {
        if (
          typeof window.webkit.messageHandlers.getUserInfo !== "undefined" &&
          typeof window.webkit.messageHandlers.getUserInfo.postMessage === "function"
        ) {
          window.webkit.messageHandlers.getUserInfo.postMessage({
            messageUDID: messageUDID,
          });
        }
      }

      postMessageToGetUserInfo(messageUDID);
    } catch (error) {
      console.error("[nativeGetUserInfo] error", error);
    }
  };

  function closeFwaWindow() {
    try {
      if (isAndroidSupported()) {
        if (typeof window.JSBridge.closeWindow === "function") {
          window.JSBridge.closeWindow();
        }
      }

      if (isIOSSupported()) {
        if (
          typeof window.webkit.messageHandlers.closeWindow !== "undefined" &&
          typeof window.webkit.messageHandlers.closeWindow.postMessage === "function"
        ) {
          window.webkit.messageHandlers.closeWindow.postMessage("closeWindow");
        }
      }

      postMessageToCloseWindow();
    } catch (error) {
      console.error("[nativeCloseNWindow] error", error);
    }
  }

  function logFwaEvent(name, params) {
    try {
      if (isAndroidSupported()) {
        if (typeof window.JSBridge.logEvent === "function") {
          window.JSBridge.logEvent(name, JSON.stringify(params));
        }
      }

      if (isIOSSupported()) {
        if (
          typeof window.webkit.messageHandlers.logEvent !== "undefined" &&
          typeof window.webkit.messageHandlers.logEvent.postMessage === "function"
        ) {
          window.webkit.messageHandlers.logEvent.postMessage({
            name: name,
            params: JSON.stringify(params),
          });
        }
      }

      postMessageToLogFwaEvent(name, params);
    } catch (error) {
      console.error("[nativeLogEvent] error", error);
    }
  }

  // Main functions
  function injectMessageHandlers() {
    if (typeof window !== "undefined") {
      window.closeFwaWindow = closeFwaWindow;
      window.logFwaEvent = logFwaEvent;

      window.setUserInfo = setUserInfo;
      window.setFWATheme = setFWATheme;
      window.setFirebaseToken = setFirebaseToken;

      window.addEventListener("message", handleReceiveFwaMessage);
    }
  }

  function doPollingUserInfo() {
    var messageUID = (new Date()).toISOString();
    var intervalTime = 5000;
    getUserInfo(messageUID);
    setInterval(function () {
      getUserInfo(messageUID);
    }, intervalTime);
  }


  injectMessageHandlers();
  doPollingUserInfo();
  // WEB-APP-HANDLERs

  // HEADER SCROLL
  function doHeaderScroll() {
    var headerWrapperDiv = null;
    // var windowHeight = window.innerHeight;
    // var documentHeight = document.body.clientHeight;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = null;

    function handleScroll() {
      if (!headerWrapperDiv) {
        headerWrapperDiv = document.getElementById("header-bar-wrapper");
      }
      if (!headerWrapperDiv) {
        return;
      }
      if (!navbarHeight) {
        navbarHeight = headerWrapperDiv.offsetHeight;
      }

      var currentScrollTop = window.pageYOffset;
      if (Math.abs(lastScrollTop - currentScrollTop) <= delta) {
        return;
      }

      // If SCROLLING DOWN -> HIDE HEADER
      if (currentScrollTop - lastScrollTop > 0 && currentScrollTop >= navbarHeight) {
        headerWrapperDiv.classList.add("nav-up");
      }
      // If SCROLLING UP -> SHOW HEADER
      if (currentScrollTop - lastScrollTop < 0) {
        headerWrapperDiv.classList.remove("nav-up");
      }
      lastScrollTop = currentScrollTop;
    }

    window.addEventListener("scroll", handleScroll);
  }

  doHeaderScroll();
  // HEADER SCROLL


  // N-PROGRESS
  function doProgressBar() {
    function onNProgressScriptLoaded() {
      NProgress.configure({
        showSpinner: false,
        template: '<div id="fwa-progress-bar" class="bar" role="bar"></div>'
      });

      var allImages = document.getElementsByTagName("img");
      var imagesCount = allImages.length;
      var loadedImageCount = 0;

      function handleOneImageLoaded() {
        loadedImageCount += 1;
        NProgress.set(loadedImageCount / imagesCount);
        if (loadedImageCount / imagesCount === 1) {
          document.body.classList.remove("is-loading");
          getUserInfo((new Date()).toISOString());
        }
      }

      NProgress.start();
      document.body.classList.add("is-loading");

      if (!imagesCount || imagesCount === 0) {
        NProgress.done();
        document.body.classList.remove("is-loading");
        getUserInfo((new Date()).toISOString());
      } else {
        for (var index = 0; index < allImages.length; index++) {
          const currentImage = allImages.item(index);
          if (currentImage.complete) {
            handleOneImageLoaded();
          } else {
            currentImage.addEventListener("load", handleOneImageLoaded, false);
          }
        }
      }
    }

    var nprogressScript = document.createElement("script");
    nprogressScript.setAttribute("src", "https://unpkg.com/nprogress@0.2.0/nprogress.js");
    document.head.appendChild(nprogressScript);
    nprogressScript.onload = onNProgressScriptLoaded;
  }

  doProgressBar();
  // N-PROGRESS


  var isOriginAgentClusterSupported = window.originAgentCluster;
  console.debug("is ORIGIN-AGENT-CLUSTER supported", isOriginAgentClusterSupported);
}
)();
